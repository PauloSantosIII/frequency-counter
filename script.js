document.getElementById("countButton").onclick = function() {  
    let typedText = document.getElementById("textInput").value
    typedText = typedText.toLowerCase()
    typedText = typedText.replace(/[^a-zà-ü'\s]+/g, "")
    countLetters(typedText)
    countWords(typedText)
}

function countLetters(typedText){
    let text = typedText.replace(/[^a-zà-ü']+/g, "")
    let letterCounts = {}
    for (let letter in text) {
        let currentLetter = text[letter]
        if (letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1 
         } else { 
            letterCounts[currentLetter]++
         }
    }
    for (let letter in letterCounts) { 
        const span = document.createElement("span")
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ")
        span.appendChild(textContent)
        span.className = "letters"
        document.getElementById("lettersDiv").appendChild(span) 
    }
    return letterCounts
}

function countWords(typedText){
    let text = typedText.split(/\s/)
    let wordCounts = {}
    for (let word in text){
        let currentWord = text[word]
        if (wordCounts[currentWord] === undefined){
            wordCounts[currentWord] = 1
        } else {
            wordCounts[currentWord]++
        }
    }

    for (let word in wordCounts) { 
        const span = document.createElement("span")
        const textContent = document.createTextNode( word + ":" + wordCounts[word] + ", ") 
        span.appendChild(textContent)
        span.className = "words"
        document.getElementById("wordsDiv").appendChild(span)
    }
    return wordCounts
}
